"use strict";

controllersModule.controller('IndivController' , ['$location', '$scope', '$routeParams' ,'ItemDataService', 'UserDataService',
     function ($location, $scope, $routeParams , ItemDataService , UserDataService) {

	$scope.name = "IndivController";
     $scope.params = $routeParams;
     $scope.indivItem = {};

     var success = function (data) {
          $scope.indivItem = data.toJSON();
     }

     var failure = function (data) {
          $location.path("/404");
     }

     ItemDataService.getById($routeParams.id).$promise.then(success,failure);// asynchronous

	$scope.isAvailable = function(){// onscreen
		if($scope.indivItem.inStock === 0){
			return false;
		}
		return true;
	};

     
   

}]);