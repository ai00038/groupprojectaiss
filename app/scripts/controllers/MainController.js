"use strict";

/**
	Our Main Controller
*/
controllersModule.controller('MainController' , 
	[ '$scope', '$route' , '$routeParams' , '$location',
		function($scope , $route , $routeParams , $location) {

	    $scope.$route = $route;
	    $scope.$location = $location;
	    $scope.$routeParams = $routeParams;

	    $scope.hello = function () {
	    	return "Hello";
	    };



 // end controller 
}]);


