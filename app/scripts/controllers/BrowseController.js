"use strict";

/**
	Our Main Controller 
*/
controllersModule.controller('BrowseController' , 
	[ '$scope' , 'ItemDataService',
		function($scope , ItemDataService) {

			$scope.itemList = ItemDataService.getAllItems();



			$scope.isAvailable = function(item){

				if(item.inStock === 0){
					return false;
				}
				return true;

			};


			$scope.filterItem = function(item){
				return $scope.filterFunction(item, [

						$scope.filterByName(item, $scope.searchName),
						$scope.filterByPriceUpper(item, $scope.searchPriceUpper),
						$scope.filterByPriceLower(item, $scope.searchPriceLower),
						$scope.filterByCategory(item , $scope.searchCategory),
						$scope.filterByDetails(item , $scope.searchDetails)

					]);
			};



			/**
				A filtering function that should be able to filter an item and return true or false based on criteria
				the filtering will be "loose", only needs to fit atleast 1 criterion to be true, for strings they don't have to be an exact match

			*/
			$scope.filterFunction = function (item,cfa) { // cfa stands for "criteria functions array" - an array of calls to functions, all of which have a criteria
				for(var i = 0; i < cfa.length ; i++){
					if(cfa[i] === false){ // is a strict "AND" search - all criteria for filter search MUST be true for filter function to return true 
						return false; // 
					}
				}
				return true;
			};

			/**
				private filtration of criteria -- all filter should allow no inputs at all, in which case if there is no input from front end, 
				it should return all possible values in that criteria 
			*/


			/**
				only partial name needs to be given to filter by name - case can be ignored
			*/
			$scope.filterByName = function(item,name){
				if(name===null || name ==="" || name === undefined){
					return true;
				}
				var _iName = item.name.toLowerCase();
				var _name = name.toLowerCase();
				return _iName.search(_name)!==-1;
			};


			$scope.filterByPriceUpper = function(item , upperBound){
				if(upperBound===null || upperBound ==="" || upperBound === undefined){
					return true;
				}
				return item.price <= upperBound;
			};

			$scope.filterByPriceLower = function(item , lowerBound){
				if(lowerBound===null || lowerBound ==="" || lowerBound === undefined){
					return true;
				}
				return item.price >= lowerBound;
			};

			/**
				filter by price, if within price range (inclusive) return true
			*/
			$scope.filterByPrice = function(item,lowerBound,upperBound){
				if(upperBound===null || upperBound ==="" || upperBound === undefined){
					return true;
				}
				return item.price >= lowerBound && item.price <= upperBound; 
			};

			/**
				filter by partial category 
			*/
			$scope.filterByCategory = function(item , category){
				if(category===null || category ==="" || category === undefined){
					return true;
				}
				var _iCategory = item.category.toLowerCase();
				var _category = category.toLowerCase();
				return _iCategory === _category;
			};

			/**
				loose search of this - can ignore case
			*/
			$scope.filterByDetails = function(item , details){
				if(details===null || details ==="" || details === undefined){
					return true;
				}
				var _iDetails = item.details.toLowerCase();
				var _details = details.toLowerCase();
				return _iDetails.search(_details)!==-1;
			};
 // end controller 
}]);