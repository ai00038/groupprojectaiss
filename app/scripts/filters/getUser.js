/**
	Get user by id
*/
app.filter('getUser', ['UserDataService', function(UserDataService){
    return function(id){
    	var user = UserDataService.getById(id);
    	return user.username;
    }
}]);