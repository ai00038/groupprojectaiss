servicesModule.service('UserDataService' , function ($resource) {
	var Users = $resource('http://localhost:3000/users/:id');
	var objectToArray = function (obj) {

		return Object.keys(obj).map(function(k){return obj[k]});

	};

	 return {
		getAllUsers : function () { // return all objs
			return Users.query();
		},

		add : function (user) {
		},

		update : function (user) {
		},

		remove : function (id) {
		},

		getById : function (id) {
			return Users.get({id: id});
		}
	}

});