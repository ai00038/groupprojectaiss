"use strict";

/**
	service to handle all data.. original service
*/

servicesModule.service('ItemDataService' , function ($resource) {
	var itemList = $resource('http://localhost:3000/items/:id');

	var objectToArray = function (obj) {

		return Object.keys(obj).map(function(k){return obj[k]});

	};

	 return {
		getAllItems : function () { // return all objs
			return itemList.query();
		},

		add : function (item) {
		},

		update : function (item) {
		},

		remove : function (id) {// deletes from backend list?
		},

		getById : function (id) {
			return itemList.get({id: id});
		}
	}

});



/**
	
servicesModule.service('thisToDoService' , function ($resource) {
	var Todo = $resource('http://lapbtn11906:8090/api/todos');

	 return {
		getAllTasks : function () { // return all objs
			return Todo.query();
		},

		add : function (task) {
		},

		update : function (task) {
		},

		remove : function (id) {// deletes from backend list?
			return Todo.delete({id: todoId});
		},

		getById : function (todoId) {
			return Todo.get({id: todoId});
		}
	}

});

*/