/**
	start project
*/

// services module
var servicesModule = angular.module('servicesModule',[]);

/** controllers module */
var controllersModule = angular.module('controllersModule',['servicesModule']);



// app module.. dependent on controllers
var app = angular.module(
	'AwesomeApp', [
		'controllersModule',
		'ngRoute',
		'ngResource',
		'ngMessages'
	]
).config( function ($routeProvider){// config for ng route... how it needs to route for diff pages.. navigation within spa
			//essentially changing views here
		$routeProvider
			.when('/', {
				controller : 'MainController',
				templateUrl : 'app/views/home.html'
			})
			.when('/browse', { 
				controller : 'BrowseController',
				templateUrl : 'app/views/browse.html'
			})
			.when('/browse/:id',{// to see individual items with descriptions, reviews and ratings..  can also buy from here
				controller : 'IndivController',
				templateUrl : 'app/views/item.html'
			})
			.when('/account' ,{// sign in or register
				templateUrl : 'app/views/account.html'
				
			})
			.when('/basket' , {// view items currently in basket

			})
			.when('/404' , { // for 404 errors
				templateUrl : 'app/views/error404.html'
			});

	}
);