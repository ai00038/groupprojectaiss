/**
	Need a mock Item data service that we can reuse for testing purposes
*/
var mockItemDataService = function () {
	
	var itemList = {
		latestId:4,
		items : {

			1 : {
				id:1,
				name:"Good Shoes",
				price:25,
				details:"seashell details",
				category:"Cool",
				imgUrl:"mockItem1.jpg",
				inStock: 1
			},

			2 : {
				id:2,
				name:"Bad Shoes",
				price:50,
				details:"shesells details",
				category:"UnCool",
				imgUrl:"mockItem2.jpg",
				inStock : 0,
			},

			3 : {
				id:3,
				name:"Good Gloves",
				price:75,
				details:"on the seashore",
				category:"Cool",
				imgUrl:"mockItem3.jpg"
			},

			4 : {
				id:4,
				name:"Bad Gloves",
				price:100,
				details:"on the wee shore",
				category:"UnCool",
				imgUrl:"mockItem4.jpg"
			}

		}
	};

	var objectToArray = function (obj) {

		return Object.keys(obj).map(function(k){return obj[k]});

	};

	 return {
		getAllItems : function () { // return all objs
			return objectToArray(itemList.items);
		},

		add : function (item) {
			var id = itemList.latestId + 1; // increment latestId
			itemList.latestId = id; // update incremented id
			item.id = id; // give task latest id
			itemList.items[id] = item;
			
			return id;
		},

		update : function (item) {
			itemList.items[item.id] = item;
		},

		remove : function (id) {// deletes from backend list?
			delete itemList.items[id];
		},

		getById : function (id) {
			return itemList.items[id];
		}
	}
};