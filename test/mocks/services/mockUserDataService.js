'use strict';

var mockUserDataService = function () {

	var userList = {

		
		users : {

			latestId:2,

			1 : {
				userid: 1,
				username : "customer1",
				password : "password1",
				firstName : "John",
				lastname : "Doe",
				address : "123 Fake Street"
			},

			2 : {
				userid: 2,
				username : "customer2",
				password : "password2",
				firstName : "Joe",
				lastname : "Bloggs",
				address : "3rd Floor Cottons Centre"
			}

		}

	};

	var objectToArray = function (obj) {

		return Object.keys(obj).map(function(k){return obj[k]});

	};

	 return {
		getAllUsers : function () { // return all objs
			return objectToArray(userList.users);
		},

		add : function (user) { // adds to list of users
			var id = userList.latestId + 1; // increment latestId
			userList.latestId = id; // update incremented id
			user.id = id; // give task latest id
			userList.users[id] = user;
			
			return id;
		},

		update : function (user) {
			userList.users[user.id] = user;
		},

		remove : function (id) {
			delete userList.users[id];
		},

		getById : function (id) {
			return userList.users[1];
		}
	}
};