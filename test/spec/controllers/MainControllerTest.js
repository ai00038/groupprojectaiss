/*
	Test Suite 
*/

describe('Testing Main Controller of App' , function (){
/*

	beforeEach(function () {
		module('AwesomeApp');
	});
*/

	beforeEach( function () {
		angular.module('servicesModule',[]); // controllersModule is dependent on something called 'servicesModule' so we create one here so it's avaliable when we create the new module bellow
		
		module('controllersModule'); // your controller lives in this module

	});

	var MainController , scope , route , routeParams , location;

	// init controller and (mock) scope

	//$scope , $route , $routeParams , $location
	beforeEach(inject(function ($controller, $rootScope){
		scope = $rootScope.$new();
		scope.route = route;
		scope.location = location;
	    scope.routeParams = routeParams;
		MainController = $controller('MainController' , {
			$scope: scope,
			$route:route,
			$location:location,
			$routeParams:routeParams
		});

	}));
	

	it('Should Pass' , function (){
		expect(1).toEqual(1);
	});

	it('Should return hello' , function () {
		expect(scope.hello()).toEqual("Hello");
	});


});