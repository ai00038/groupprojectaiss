describe('Testing Browse Controller of App' , function (){
/*

	beforeEach(function () {
		module('AwesomeApp');
	});
*/


	beforeEach( function () {
		angular.module('servicesModule',[]); // controllersModule is dependent on something called 'servicesModule' so we create one here so it's avaliable when we create the new module bellow
		
		module(function($provide) { // assign mock service
			$provide.service('ItemDataService', mockItemDataService);
		});


		module('controllersModule'); // your controller lives in this module

		


	});

	var BrowseController , scope;

	// init controller and (mock) scope

	
	beforeEach(inject(function ($controller, $rootScope){
		scope = $rootScope.$new();
		BrowseController = $controller('BrowseController' , {
			$scope: scope
		});

	}));
	
	/**
		Just a quick check to make sure our mock data service works
	*/
	it('should retrieve correct item' , function (){
		expect(scope.itemList[0]).toEqual({
				id:1,
				name:"Good Shoes",
				price:25,
				details:"seashell details",
				category:"Cool",
				imgUrl:"mockItem1.jpg",
				inStock:1
			});
	});

	it('Should filter out an item by exact name' , function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByName(scope.itemList[0],"Good Shoes")
		])).toEqual(true);
	});

	it('Should filter out an item by exact name - expect false return' , function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByName(scope.itemList[0],"Bad Shoes")
		])).toEqual(false);
	});
	
	it('Should filter out an item by partial name' , function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByName(scope.itemList[0],"Shoes")
		])).toEqual(true);
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByName(scope.itemList[0],"Shoes")
		])).toEqual(true);
	});

	it('Should filter out an item by price' , function(){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByPrice(scope.itemList[0], 0, 25)
		])).toEqual(true);
	})

	it('Should filter out an item by price - but return false' , function(){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByPrice(scope.itemList[0], 0, 24.9)
		])).toEqual(false);
	})

	it('Should filter out an item by partial name AND price - and return true', function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByPrice(scope.itemList[0] , 0 , 25),
			scope.filterByName(scope.itemList[0], "Shoes")
		])).toEqual(true);
	});

	it('Should filter out an item by partial name AND price - and return false' , function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByPrice(scope.itemList[0] , 0 , 25),
			scope.filterByName(scope.itemList[0], "France") // doesn't match name
		])).toEqual(false);
	})

	it('Should filter out an item by exact category - and return true' , function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByCategory(scope.itemList[0],"Cool")
		])).toEqual(true);	
	});


	it('Should filter out an item by exact category - and return false' , function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByCategory(scope.itemList[0],"UnCool")
		])).toEqual(false);	
	});

	it('Should filter out an item by partial name, price AND exact category - and return true' , function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByCategory(scope.itemList[0],"Cool"),
			scope.filterByPrice(scope.itemList[0] , 0 , 25),
			scope.filterByName(scope.itemList[0],"oes")
		])).toEqual(true);
	});

	it('Should filter out an item by partial name, price AND exact category - and return false' , function(){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByCategory(scope.itemList[0],"Cool"),
			scope.filterByPrice(scope.itemList[0] , 0 , 25),
			scope.filterByName(scope.itemList[0],"Shoos") // only incorrect field
		])).toEqual(false);	
	});

	it('Should filter out an item by partial details - and return true' , function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByDetails(scope.itemList[0] , "hell")
		])).toEqual(true);
	});

	it('Should filter out an item by partial details - and return false' , function (){
		expect(scope.filterFunction(scope.itemList[0],[
			scope.filterByDetails(scope.itemList[0] , "smell")
		])).toEqual(false);
	});

	it('Should filter out an item by partial name, price, exact category AND partial details' , function (){
		expect(scope.filterFunction(scope.itemList[1],[
			scope.filterByDetails(scope.itemList[1],"sells"),
			scope.filterByName(scope.itemList[1],"Bad"),
			scope.filterByPrice(scope.itemList[1],25,100),
			scope.filterByCategory(scope.itemList[1],"UnCool")
		])).toEqual(true);
	});

	it('Should filter out an item by partial name, price, exact category AND partial details' , function (){
		expect(scope.filterFunction(scope.itemList[1],[
			scope.filterByDetails(scope.itemList[1],"ells"),
			scope.filterByName(scope.itemList[1],"Bad"),
			scope.filterByPrice(scope.itemList[1],25,100),
			scope.filterByCategory(scope.itemList[1],"Cool")
		])).toEqual(false);
	});

	//filtering prices indiv
	it('filter item by price but using the seperate price filters', function (){
		expect(scope.filterFunction(scope.itemList[2],[
			scope.filterByPriceUpper(scope.itemList[2],75),
			scope.filterByPriceLower(scope.itemList[2],75)
		])).toEqual(true);
	});

	it('should understand that the item is in stock' , function (){
		expect(scope.isAvailable(scope.itemList[0])).toEqual(true);
	});

	it('should understand that the item is not in stock' , function (){
		expect(scope.isAvailable(scope.itemList[1])).toEqual(false);
	});
});