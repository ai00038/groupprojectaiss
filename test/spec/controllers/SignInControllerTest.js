'use strict';

describe('Controller: SignInController' , function (){

	beforeEach(function () {
		module('AwesomeApp');

		// create a mock service
		module(function ($provide) {
			$provide.service('UserDataService', mockUserDataService);
		});
	});

	var SignInController, scope;

	// init controller and (mock) scope

	beforeEach(inject(function ($controller, $rootScope, _UserDataService_) {
		scope = $rootScope.$new();
		SignInController = $controller('SignInController' , {
			$scope: scope,
			UserDataService: _UserDataService_
		});

	}));

	it('should pass with 1 equal to 1' , function (){
		expect(1).toEqual(1);
	});

	it('should ', function () {});

});