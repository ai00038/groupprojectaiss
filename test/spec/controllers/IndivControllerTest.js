"use strict";

describe('Controller : IndivController' , function (){

	beforeEach( function () {
		angular.module('servicesModule',[]); // controllersModule is dependent on something called 'servicesModule' so we create one here so it's avaliable when we create the new module bellow
		
		module(function($provide) { // assign mock service
			$provide.service('ItemDataService', mockItemDataService);
		});

		module('controllersModule'); // your controller lives in this module

	});

	var IndivController , scope , routeParams;

	// init controller and (mock) scope

	
	beforeEach(inject(function ($controller, $rootScope){
		scope = $rootScope.$new();
		routeParams = {
			id:1
		};// giving a mock routeParams
		scope.routeParams = routeParams;
		IndivController = $controller('IndivController' , {
			$scope: scope,
			$routeParams : routeParams
		});

	}));





});
